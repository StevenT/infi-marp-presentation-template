---
marp: true
paginate: true
header: '
<div>
  ![h:50 w:50](images/astro-framework.svg)
  <div>**Astro** - _The one ring to rule them all_</div>
</div>
<div>
  <div>_Steven Thonus_</div>
  ![h:50 w:50](images/steven-transparent.png)
</div>
'
---

![h:350 center](images/astro-seeklogo.com.svg)

### The one ring to rule them all

---

# Astro?

* Astro is an all-in-one web framework for building fast, content-focused websites.
* Pfff, nòg een frontend framework erbij... 🤦
* ![why h:400](images/why-meme.png)

---

# Astro is…

* Content-focused 📄
* Server-first 🖥️
* Fast by default 🚀
* Easy to use 👌
* Fully-featured 📦

---

# Content-focused

* For content-rich websites:
  * Most marketing sites
  * Publishing sites
  * Documentation sites
  * Blogs
  * Portfolios

---

# Server-first

* Server-side over client-side rendering
* Different than others (Next.js, SvelteKit, Nuxt, Remix):
  * Client-side rendering + SSR to address performance concerns

---

# Fast by default

* Zero JS, by default
* Good performance is alway important
* Especially critial for content-focused websites
  * Every 100ms faster → 1% more conversions (Mobify)
  * 50% faster → 12% more sales (AutoAnything)
  * 20% faster → 10% more conversions (Furniture Village)
  * 40% faster → 15% more sign-ups (Pinterest)
  * Every 1 second slower → 10% fewer users (BBC)

---

# Easy to use

* Accessible to every web developer 🧡
* Feel familiar and approachable
* Astro components can be written in Astro's UI language `.astro`
* HTML is valid `.astro` 👍
* Supports JSX expressions and CSS scoping
* Astro components can also be written with your preferred framework 🥳
* React, Preact, Svelte, Vue, Solid, Lit and others

---

# Fully-featured

<div class="columns">
<div>

* Component syntax
* File-based routing
* Asset handling
* A build process

</div>
<div>

* Bundling
* Optimizations
* Data-fetching
* and more

</div>

---

# Bring Your Own UI Framework

These Frameworks

<div class="columns">
<div>

* Astro
* React
* Preact
* SolidJS

</div>
<div>

* Svelte
* Vue
* Lit
* AlpineJS

</div>

are all officially supported in Astro

---

# Astro Islands

<div class="columns">
<div>

![islands](images/astro-islands.png)

</div>
<div>

* Interactive UI component on otherwise static page
* Multiple islands can exist on a page
* An island always renders in isolation
* Mix and match different frameworks on the same page

</div>
</div>

---

# More on Islands

* Astro generates every website with zero client-side JavaScript, by default
* React, Svelte, Vue components will render to HTML only
* From `Progressive Hydration` to `Seperate Hydration`
* No top down rendering!
* Optional hydrate you React, Svelte, Vue component for interactivity
  * Hydrate `on load` or `on idle` (later)
  * Hydrate `on visible`
  * Hydrate `with media query`
  * Hydrate `client only`

---

# Why these Islandz?

* Performance:
  * Most of the page is HTML + CSS
  * JavaScript is only loaded for the individual components that need it
  * Parallel loading so 1 slow loading component does not block another
* Site from multiple projects / teams
* Runtime shared components
* Migration in steps

---

# Demo

<span style="display: flex; flex-direction: column; align-items: center">
  Astro demo
  <a href="http://localhost:4321" target="_blank">http://localhost:4321</a>
</span>

<br/>

<span style="display: flex; flex-direction: column; align-items: center">
  Transitions demo video
  <a href="http://localhost:3000/transition-demo.mp4" target="_blank">http://localhost:3000/transition-demo.mp4</a>
</span>

---

# Questions?

<span style="width: 30%; display: inline-block;">**Astro**</span><https://astro.build>

<span style="width: 30%; display: inline-block;">**Islands Architecture**</span><https://jasonformat.com/islands-architecture>
_by Jason Format_

